import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "stacksimple.settings")

from django.core.management import call_command
from haystack.query import SearchQuerySet

django.setup()

call_command('rebuild_index', interactive=False, verbosity=0)

for item in SearchQuerySet().auto_query('lorem').all():
    print item.object
# print SearchQuerySet().all()