import datetime
from haystack import indexes
from . import models as frontend_models

class PersonIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    person_name = indexes.CharField(model_attr='person_name')

    def get_model(self):
        return frontend_models.Person

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return frontend_models.Person.objects.all()