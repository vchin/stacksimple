from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class Person(models.Model):
    person_name = models.CharField(verbose_name=_('person name'), max_length=200)
    score = models.FloatField(verbose_name=_('score'), default=0.0)

    def __str__(self):
        return self.person_name

    def __unicode__(self):
        return self.person_name

    class Meta:
        verbose_name = _('persons')
        verbose_name_plural = _('person')
