from haystack import forms as haystack_forms
from django.apps import apps

class ExModelSearchForm(haystack_forms.ModelSearchForm):
    def get_models(self):
        """Return an alphabetical list of model classes in the index."""
        search_models = []

        if self.is_valid():
            for model in self.cleaned_data['models']:
                search_models.append(apps.get_model(model))

        return search_models
