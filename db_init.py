# -*- coding: utf-8 -*-
import os
import random
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "stacksimple.settings")
django.setup()

from django_dynamic_fixture import G
from django.core.management import call_command
from django.contrib.auth.models import User
from django.utils import lorem_ipsum

from frontend import models as fm

def init(reset=True):
    if reset:
        call_command('reset_db', interactive=0)

    call_command('makemigrations')
    call_command('migrate')

    admin = G(User, username='admin', is_active=True, is_staff=True, is_superuser=True)
    admin.set_password('1')
    admin.save()

    for i in xrange(10):
        G(fm.Person, person_name = lorem_ipsum.words(random.randint(10, 20)), score=random.random() * 100.0)


if __name__ == '__main__':
    init()
