Django==1.9
django-dynamic-fixture==1.8.5
django-extensions==1.6.1
django-haystack==2.4.1
pefile==1.2.10.post114
python-ptrace==0.8.1
six==1.10.0
wheel==0.24.0
